const GUEST_ROUTES = ["login"];

export default function({ store, route, redirect }) {
  const isAuthenticated = store.getters["auth/loggedIn"];
  if (route.name === "index") return;
  if (isAuthenticated && GUEST_ROUTES.includes(route.name)) {
    return redirect("/");
  } else if (!isAuthenticated && !GUEST_ROUTES.includes(route.name)) {
    return redirect("/login");
  }
}
