export default {
    loading: state => state.loading,
    error: state => state.error,
    loggedIn: state => state.loggedIn
}