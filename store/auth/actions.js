import ApiService from "~/services/api.service";
import TokenService from "~/services/storage.service";
export default {
  setLogin({ commit }, payload) {
    commit("setLoading", true);
    commit("clearError");
    TokenService.saveToken("fnqrQ4Qm4DuTBMcCZSMs");
    ApiService.setHeader();
    commit("setLoggedIn", true);
    commit("setLoading", false);
    this.$router.push("/");
  },
  logout({ commit }) {
    TokenService.removeToken();
    ApiService.removeHeader();
    commit("setLoggedIn", false);
    this.$router.push('/login')
  },
  clearError({ commit }) {
    commit("clearError")
  }
};