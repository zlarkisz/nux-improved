export default () => ({
    loggedIn: false,
    loading: false,
    error: null
})