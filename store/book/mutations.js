export default {
    setItems(state, items) {
        state.items = items
    },
    setItem(state, item) {
        state.item = item
    },
    setChapters(state, chapters) {
        state.chapters = chapters
    }
}