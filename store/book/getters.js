export default {
    items: state => state.items,
    item: state => state.item,
    chapters: state => state.chapters
}