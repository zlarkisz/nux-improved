import service from "~/services/book.services";

export default {
  async fetchItems({ commit }) {
    try {
      commit("auth/setLoading", true, { root: true });
      const {
        data: { docs }
      } = await service.fetchItems({ params: {} });
      commit("setItems", docs);
      commit("auth/setLoading", false, { root: true });
    } catch (e) {
      console.log(e.message);
    }
  },
  async fetchItem({ commit }, id) {
    try {
      const { data } = await service.fetchItem({ id, params: {} });
      commit("setItem", data);
    } catch (e) {
      console.log(e.message);
    }
  },
  async fetchChapters({ commit }, id) {
    try {
      const {
        data: { docs }
      } = await service.fetchChapters({ id, params: {} });
      commit("setChapters", docs);
    } catch (e) {
      console.log(e.message);
    }
  }
};
