import TokenService from "~/services/storage.service";
import ApiService from "~/services/api.service";
const BASE_URL = "https://the-one-api.herokuapp.com/v1";
// If token exists set header
export default function({ store }) {
  if (process.browser) {
    ApiService.init(BASE_URL);
    if (TokenService.getToken()) {
      ApiService.setHeader();
      store.commit("auth/setLoggedIn", true);
    }
  }
}