import ApiService from "./api.service";

export default {
  fetchItems: ({ params }) =>
    ApiService.get({
      url: "/movie",
      params
    }),
  fetchItem: ({ id, params }) =>
    ApiService.get({
      url: `/movie/${id}`,
      params
    })
};
