import ApiService from "~/services/api.service";

export default {
  fetchItems: ({ params }) =>
    ApiService.get({
      url: "/quote",
      params
    })
};
