import ApiService from "./api.service";

export default {
  fetchItems: ({ params }) =>
    ApiService.get({
      url: "/book",
      params
    }),
  fetchItem: ({ id, params }) =>
    ApiService.get({
      url: `/book/${id}`,
      params
    }),
  fetchChapters: ({ id, params }) =>
    ApiService.get({
      url: `/book/${id}/chapter`,
      params
    })
};
