import ApiService from "./api.service";

export default {
  fetchItems: ({ params }) =>
    ApiService.get({
      url: "/character",
      params
    }),
  fetchItem: ({ params, id }) =>
    ApiService.get({
      url: `/character/${id}`,
      params
    })
};
